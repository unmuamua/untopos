## example

Let `E` be a topos.
Define three double categories, all with objects being posets in `E`, considered to be thin internal categories.
Name them after types of 1-cells: `[F] (A -> B)` is functors between `A` and `B`, `[R] (A -> B)` is relations (profunctors).

- `[F,F]` - comes from the quintet construction over the (locally thin) bicategory of functors between posets and transformations between functors:
  - H 1-cells: functors
  - V 1-cells: functors
  - HV 2-cells: squares, "commutative on the nose".
- `[R,R]` - comes from the quintet construction over the (locally thin) bicategory of profunctors between posets and relations between profunctors:
  - H 1-cells: profunctors
  - V 1-cells: profunctors
  - HV 2-cells: squares, "commutative on the nose".
- `[F,R]` - the "equipment"
  - H 1-cells: profunctors
  - V 1-cells: functors
  - HV 2-cells: squares, being maps between profunctors, along functors given.

We see, that `[F,F]` and `[R,R]` have coinciding types of 1-cells and inherent *connection* structure between them, and in this sence are degenerate.

Also we see `[F,R]` has a *framed bicategory* or *quipment* structure, as far as Yoneda structure.

The equipment structure mentioned leads to two consecutive double functors between double categories (which is identity on objects):

```
Q: [F,F] -> [F,R] -> [R,R]
```

The Yoneda structure leads to the other two consecutive double functors between double categories in the opposite direction:

```
P: [R,R] -> [F,R] -> [F,F]
```

which are adjoint to the first sequence (here `P` is in honor of the power-object functor from the topos theory).

We propose such adjunctions as a good generalization of the notion of topos to monoidal cubical categories.

Given triple of monoidal double categories `[R,R], [F,R], [F,F]` with shared objects and tensoring `[*]` and the `[R]` and `[F]` have their own exponents against `[*]` being `RExp`, `FExp` and an adjunction `Q -| P` between them.
Then if `Q` acts on object by tensoring `Q(A) = M [*] A`, we obtain a sequence of equivalences

```
[F] (A -> FExp(B, P(C)))
[F] (A [*] B -> P(C))
[R] (Q(A) [*] B -> C)
[R] (Q(A) -> RExp(B, C))
[F] (A -> P(RExp(B, C)))
```

so `FExp(B, P(C)) ~ P(RExp(B, C))`.
If `A ~ RExp(A*, I)`, then `P(A) ~ FExp(A*, N)`, where `N := P(I)` is the classifier.

TODO: check the axioms of Yoneda structure (in AGDA).

TODO: write a sketch of definitions in terms of simplicial endofunctors of the category of toposes.
